package com.example.qaautohw4.pages

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isEnabled
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.example.qaautohw4.R

class LoginPage {
    fun setUserName(name: String): LoginPage {
        onView(withId(R.id.username)).perform(typeText(name))
        return this
    }

    fun setUserPassword(password: String): LoginPage {
        onView(withId(R.id.password)).perform(typeText(password))
        return this
    }

    fun checkLoginButtonIsEnable(): LoginPage {
        onView(withId(R.id.login)).check(matches(isEnabled()))
        return this
    }
}