package com.example.qaautohw4

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.qaautohw4.pages.LoginPage
import com.example.qaautohw4.ui.login.LoginActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

var NAME: String = "Maxim"
var PASSWORD: String = "123456"

@RunWith(AndroidJUnit4::class)
class LoginTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(LoginActivity::class.java)

    @Test fun checkLoginButtonEnable() {
        LoginPage().setUserName(NAME)
            .setUserPassword(PASSWORD)
            .checkLoginButtonIsEnable()
    }

}